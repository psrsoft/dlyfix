#!/usr/bin/env python
import psrchive
import sys

mk2table=[]
mk2table.append((57850,57950))
mk2table.append((57985,58062))
mk2table.append((58249,58410))
mk2table.append((58515,58545))
mk2table.append((58579,58781.5))
mk2table.append((58997.57986111111,59102.708333333336)) # 2020-05-28 (1355 UT) — 2020-09-10 (end of working day)
mk2table.append((59128.0,59137.54861111111)) # 2020-10-06??? - 2020-10-15 (1310 UT)
mk2table.append((59155.71527777778,59156.61111111111)) # 2020-11-02 (1710 UT) — 2020-11-03 (1440 UT)
mk2table.append((59178.68402777778,59205.395833333336)) # 2020-11-25 (1625 UT) — 2020-12-22 (0930 UT)


mk2site="JB_MKII"
lovellsite="Jodrell"

for fn in sys.argv[1:]:
    ar = psrchive.Archive_load(fn)
    site = ar.execute("edit site").strip().split("=")[1]
    #frontend = ar.execute("edit rcvr:name").strip().split("=")[1]
    mjd = float(ar.get_mjds()[0])
    #print(site,mjd)
    should_be_mk2=False
    for s,f in mk2table:
        if mjd > s and mjd < f:
            should_be_mk2=True
            break

    if should_be_mk2 and site!=mk2site:
            print("{} at {} was '{}' should be '{}'".format(fn,mjd,site,mk2site))
            ar.execute("edit site={}".format(mk2site))
            ar.unload()

    if not should_be_mk2 and site==mk2site:
            print("{} at {} was '{}' should be '{}'".format(fn,mjd,site,lovellsite))
            ar.execute("edit site={}".format(lovellsite))
            ar.unload()
